'''
Complete the function to meet the following requirements
* This will be accomplished three times using three different packages (os, glob and subprocess)
* The filename starts with "file" and the has a "txt" extension
- recursively search the additional_modules_dir directory
- Read the contents of each file
- Return the relative path and filename (with extension) of the file that contains the username "jenny" (no quotes)
- Example return: "additional_modules_dir\deep\deeper\file4839503.txt"
'''

import os
import glob
from subprocess import Popen, PIPE


def find_file_with_os():
    for root, dirs, files in os.walk("."):
        for f in files:
            filepath = os.path.join(root, f)
            with open(filepath) as file_handle:
                if file_handle.readline().startswith("jenny"):
                    return filepath if filepath else None


def find_file_with_glob():
    for filepath in glob.glob("**/file*.txt", recursive=True):
        with open(filepath) as file_handle:
            if file_handle.readline().startswith("jenny"):
                return filepath if filepath else None


def find_file_with_subprocess():
    # assume we are on windows
    sp = Popen(["powershell.exe", "gci -r additional_modules_dir | select-string jenny"], stdout=PIPE)
    return sp.stdout.read().decode().strip("\r\n").split(":")[0]
