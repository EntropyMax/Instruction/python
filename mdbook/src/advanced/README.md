## Advanced

### **Introduction:**
This lesson you will quickly dive into more advanced uses and features of Python.

### **Topics Covered:**

  * [**CTypes and Structures**](ctypes.html#ctypes-and-structures-1)
  * [**Loading Libraries**](ctypes.html#loading-libraries)
  * [**Structures**](ctypes.html#structures)
  * [**Regular Expressions**](regular_expressions.html#regular-expressions)
  * [**Additional Libraries and Modules**](additional_libraries_modules.html)
  * [**Multithreading**](multithreading.html#multithreading)
  * [**Unit Testing**](unit_testing.html#unittesting)
  * [**Metaclasses**](metaclasses.html#metaclasses)

#### To access the Advanced Python slides please click [here](slides)
