## **Python Virtual Environments** 

Virtual environments are used in Python to allow different requirements to exist and different dependencies to be used in different Python projects on the same computer. 
For example, a developer may start creating a project using Foo version 0.5. The project may take many months to complete or be ongoing. However, in the meantime the commands and functionality of Foo are radically changed with the introduction of Foo versions 1.0 and 2.0. 
In order that the developer can continue to use Foo version 0.5, she would set up a virtual environment and install version 0.5. This would not affect other environments using later versions of Foo. 
Virtual environments are easy to install and use in Python.
```Python
pip3 install --user virtualenv
```
Then in the project directory, simply use to initialize the Python virtual environment.
```Python
python3 -m venv env
```

This will create a separate Python install for the project. To begin using that particular Python virtual environment, simply type: source env/bin/activate
Then you will be in the virtual Python environment. To stop using the virtual environment, simply type: deactivate

See also: [here](https://packaging.python.org/guides/installing-using-pip-and-virtual-environments/). 

### **Exporting and Importing Python Virtual Environments**

In order to create a Python virtual environment,
use `pip` i.e. `pip3` to get a list of requirements:
```bash
pip3 freeze > requirements.txt
```
Create your environment:
```bash
virtualenv my_env
```
Start using the environment:
```bash
source my_env/bin/activate
```
Use `pip` to install the requirements:
```bash
pip3 install -r requirements.txt
```


See also: [here](https://stackoverflow.com/questions/14684968/how-to-export-virtualenv) and for alternatives see [here](https://python-docs.readthedocs.io/en/latest/dev/virtualenvs.html)








