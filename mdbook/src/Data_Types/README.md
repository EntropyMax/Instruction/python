## Data Types

### **Introduction:**
Python uses several data types to store different variables as that all have different functionality. In this lesson you will go over the use and structure of the different data types.

### **Topics Covered:**

* [**Variables**](variables.html#variables)
* [**Numbers**](numbers.html#numbers)
* [**Strings**](strings.html#strings)
* [**Lists**](lists.html#lists)
* [**Bytes and Bytearray**](byte_array.html#bytes-and-bytearray)
* [**Tuples**](tuples.html#tuples-range--buffer) 
* [**Range**](tuples.html#range) 
* [**Buffer**](tuples.html#buffer-memoryview)
* [**Dictionaries**](mapping.html#dictionaries-and-sets)
* [**Sets**](mapping.html#set-and-frozenset)
* [**Conversion Functions**](mapping.html#additional-functionality)

#### To access the Data Types slides please click [here](slides/)
